<?php
/**
* $node: Node element.
* $content : Node content.
* $content['field_name'] : Node field element.
* Some variables was defined in function md_boom_preprocess_node from inc/template.node.process.inc file
*/
?>
<?php

$current_path = current_path();
$multimedia = $content['field_portfolio_multimedia']['#items'];
$count = count($multimedia);
$media_content = '';
foreach($multimedia as $key => $value){
    $file_type = $value['file']->type;
    $image = file_create_url($value['file']->uri);
    $image_uri = $value['file']->uri;
    $image_alt = $value['file']->alt;
    $image_title = $value['file']->title;
    if($file_type != 'image') {
        $media_content .= '<li>'.render($content['field_portfolio_multimedia'][$key]).'</li>';
    } else {
        $media_content .= '<li><img src="'.image_style_url('portfolio_preview',$image_uri).'"';
        $media_content .= 'alt="'.$image_alt.'"/>';
        $media_content .= '<div class="pf-content"><p>'.$image_title.'</p></div></li>';
    }
}
if(isset($node->field_portfolio_taxonomy[$node->language])){
  $taxonomy = $node->field_portfolio_taxonomy[$node->language];
}
if($taxonomy != null) {
    $taxonomy_output = '';
    if(count($taxonomy) > 1) {
        $current = $taxonomy;
        end($taxonomy);
        $last_key = key($taxonomy);
        unset($taxonomy[$last_key]);
        foreach ($taxonomy as $key => $value){
            $taxonomy_output .= '<a href="'.url('taxonomy/term/'.$value['tid'].'').'">'.$value['taxonomy_term']->name.'</a>, ';
        }
        $taxonomy_output .= '<a href="'.url('taxonomy/term/'.$value['tid'].'').'">'.$current[$last_key]['taxonomy_term']->name.'</a>';
    } else {
        $taxonomy_output .= '<a href="'.url('taxonomy/term/'.$taxonomy[0]['tid'].'').'">'.$taxonomy[0]['taxonomy_term']->name.'</a>';
    }
}


if (isset($node->field_byggiar[$node->language])){
$byggiar = $node->field_byggiar[$node->language];
if($byggiar != null) {
    $byggiar_output = '';
    $byggiar_output .= $byggiar[0]['value'];
  }
}
if (isset($node->field_ibudir[$node->language])){
$ibudir = $node->field_ibudir[$node->language];
if($ibudir != null) {
    $ibudir_output = '';
    $ibudir_output .= $ibudir[0]['value'];
  }
}
if (isset($node->field_buvidd[$node->language])){
$buvidd = $node->field_buvidd[$node->language];
if($buvidd != null) {
    $buvidd_output = '';
    $buvidd_output .= str_replace(' ','',$buvidd[0]['value']);
  }
}
if (isset($node->field_rum[$node->language])){
$rum = $node->field_rum[$node->language];
if($rum != null) {
    $rum_output = ''; 
    $rum_output .= str_replace(' ','',$rum[0]['value']);
  }
}
if (isset($node->field_felagsrum_hus[$node->language])){
$felagsrum = $node->field_felagsrum_hus[$node->language];
if($felagsrum != null) {
    $felagsrum_output = '';
    $felagsrum_output .= strtolower($felagsrum[0]['value']);
  }
}
if (isset($node->field_goymslurum[$node->language])){
$goymslurum = $node->field_goymslurum[$node->language];
if($goymslurum != null) {
    $goymslurum_output = '';
    $goymslurum_output .= strtolower($goymslurum[0]['value']);
  }
}
if (isset($node->field_hjallur[$node->language])){
$hjallur = $node->field_hjallur[$node->language];
if($hjallur != null) {
    $hjallur_output = '';
    $hjallur_output .= strtolower($hjallur[0]['value']);
  }
}
if (isset($node->field_altan[$node->language])){
$altan = $node->field_altan[$node->language];
if($altan != null) {
    $altan_output = '';
    $altan_output .= strtolower($altan[0]['value']);
  }
}if (isset($node->field_haeddir[$node->language])){
$haeddir = $node->field_haeddir[$node->language];
if($haeddir != null) {
    $haeddir_output = '';
    $haeddir_output .= str_replace(' ','',$haeddir[0]['value']);
  }
}
if (isset($node->field_lift[$node->language])){
$lift = $node->field_lift[$node->language];
if($lift != null) {
    $lift_output = '';
    $lift_output .= strtolower($lift[0]['value']);
  }
}
if (isset($node->field_arbeidstakari[$node->language])){
$arbeidstakari = $node->field_arbeidstakari[$node->language];
if($arbeidstakari != null) {
    $arbeidstakari_output = '';
    $arbeidstakari_output .= str_replace(' ','',$arbeidstakari[0]['value']);
  }
}
if (isset($node->field_orkukelda[$node->language])){
$orkukelda = $node->field_orkukelda[$node->language];
if($orkukelda != null) {
    $orkukelda_output = '';
    $orkukelda_output .= $orkukelda[0]['value'];
  }
}
if (isset($node->field_husdjor[$node->language])){
  $husdjor = $node->field_husdjor[$node->language];
  if($husdjor != null) {
      $husdjor_output = '';
      $husdjor_output .= strtolower($husdjor[0]['value']);
  }
}
if (isset($node->field_koliskap[$node->language])){
  $koliskap = $node->field_koliskap[$node->language];
  if($koliskap != null) {
      $koliskap_output = '';
      $koliskap_output .= strtolower($koliskap[0]['value']);
  }
}
if (isset($node->field_ovnur[$node->language])){
  $ovnur = $node->field_ovnur[$node->language];
  if($ovnur != null) {
      $ovnur_output = '';
      $ovnur_output .= strtolower($ovnur[0]['value']);
  }
}
if (isset($node->field_eymhetta[$node->language])){
  $eymhetta = $node->field_eymhetta[$node->language];
  if($eymhetta != null) {
      $eymhetta_output = '';
      $eymhetta_output .= strtolower($eymhetta[0]['value']);
  }
}
if (isset($node->field_mikroovnur[$node->language])){
  $mikroovnur = $node->field_mikroovnur[$node->language];
  if($mikroovnur != null) {
      $mikroovnur_output = '';
      $mikroovnur_output .= strtolower($mikroovnur[0]['value']);
  }
}
if (isset($node->field_uppvaskimaskina[$node->language])){
  $uppvaskimaskina = $node->field_uppvaskimaskina[$node->language];
  if($uppvaskimaskina != null) {
      $uppvaskimaskina_output = '';
      $uppvaskimaskina_output .= strtolower($uppvaskimaskina[0]['value']);
  }
}
if (isset($node->field_vaskimaskina[$node->language])){
  $vaskimaskina = $node->field_vaskimaskina[$node->language];
  if($vaskimaskina != null) {
      $vaskimaskina_output = '';
      $vaskimaskina_output .= strtolower($vaskimaskina[0]['value']);
  }
}
if (isset($node->field_t[$node->language])){
  $turkitrumla = $node->field_t[$node->language];
  if($turkitrumla != null) {
      $turkitrumla_output = '';
      $turkitrumla_output .= strtolower($turkitrumla[0]['value']);
  }
}
if (isset($node->field_leiga[$node->language])){
  $leiga = $node->field_leiga[$node->language];
  if($leiga != null) {
      $leiga_output = '';
//      $leiga_output .= str_replace(' ','',$leiga[0]['value']);
      $leiga_output .= $leiga[0]['value'];
  }
}









?>
<article class="pf-detail-item span12 clearfix">
    <?php
    hide($content['field_portfolio_taxonomy']);
    hide($content['field_portfolio_location']);
    hide($content['links']);
    hide($content['comments']);
    hide($content['field_portfolio_thumbnail']);
    hide($content['field_portfolio_layout_mode']);
    hide($content['field_portfolio_description']);

    ?>
    <?php if(isset($media_content)){?>
        <div class="pf-slider-wrapper">
            <div class="flexslider" id="pf-list-flex">
                <ul class="slides">
                    <?php print $media_content;?>
                </ul>
            </div>
        </div><!--bp-flex-->
    <?php } else { ?>
    <div class="pf-slider-wrapper">
        <?php print render($content['field_portfolio_thumbnail']);?>
    </div>

    <?php } ?>
    <div class="pf-content">
       <h3><span id="node-title"><span class="node-title"><?php print $node->title;?></span></span></h3>
        <p><?php print render($content['field_portfolio_description']);?></p>
        <div class="pf-detail-meta">
<table><tr><td valign=top>
            <div><span class="pf-meta-label"><?php print t('Slag');?></span><span class="pf-meta-info"><?php if(isset($taxonomy_output)) : print $taxonomy_output;endif;?></span></div>
            <?php /* <p><span class="pf-meta-label"><?php print t('Location');?></span><span class="pf-meta-info"><?php print render($content['field_portfolio_location']);?></span></p> */ ?>
            <?php /* <p><span class="pf-meta-label"><?php print t('Skills');?></span><span class="pf-meta-info skill-output"><?php if(isset($skill_output)) : print $skill_output;endif;?></span></p> */ ?>
            <?php if(isset($byggiar_output)) : print t('<p><span class="pf-meta-label"> Byggi&aacute;r</span><span class="pf-meta-info">'); print $byggiar_output; print t('</span></p>');endif;?>
            <?php if(isset($ibudir_output)) : print t('<p><span class="pf-meta-label"> &Iacute;b&uacute;&eth;ir</span><span class="pf-meta-info">'); print $ibudir_output; print t('</span></p>');endif;?>
            <?php if(isset($buvidd_output)) : print t('<p><span class="pf-meta-label"> B&uacute;v&iacute;dd, m<sup>2</sup></span><span class="pf-meta-info">'); print $buvidd_output; print t('</span></p>');endif;?>
            <?php if(isset($rum_output)) : print t('<p><span class="pf-meta-label"> R&uacute;m</span><span class="pf-meta-info">'); print $rum_output; print t('</span></p>');endif;?>
            <?php if(isset($felagsrum_output)) : print t('<p><span class="pf-meta-label"> Felagsr&uacute;m</span><span class="pf-meta-info">'); print $felagsrum_output; print t('</span></p>');endif;?>
            <?php if(isset($goymslurum_output)) : print t('<p><span class="pf-meta-label"> Goymslur&uacute;m</span><span class="pf-meta-info">'); print $goymslurum_output; print t('</span></p>');endif;?>
            <?php if(isset($hjallur_output)) : print t('<p><span class="pf-meta-label"> &Uacute;th&uacute;s</span><span class="pf-meta-info">'); print $hjallur_output; print t('</span></p>');endif;?>
            <?php if(isset($altan_output)) : print t('<p><span class="pf-meta-label"> Terassa/altan</span><span class="pf-meta-info">'); print $altan_output; print t('</span></p>');endif;?>
            <?php if(isset($haeddir_output)) : print t('<p><span class="pf-meta-label"> H&aelig;dddir</span><span class="pf-meta-info">'); print $haeddir_output; print t('</span></p>');endif;?>
            <?php if(isset($lift_output)) : print t('<p><span class="pf-meta-label"> Lyfta</span><span class="pf-meta-info">'); print $lift_output; print t('</span></p>');endif;?>
</td><td valign=top>
            <?php if(isset($arbeidstakari_output_disabled)) : print t('<p><span class="pf-meta-label"> Arbei&eth;stakari</span><span class="pf-meta-info">'); print $arbeidstakari_output; print t('</span></p>');endif;?>
            <?php if(isset($orkukelda_output)) : print t('<p><span class="pf-meta-label"> Orkukelda</span><span class="pf-meta-info">'); print $orkukelda_output; print t('</span></p>');endif;?>
            <?php if(isset($husdjor_output)) : print t('<p><span class="pf-meta-label"> H&uacute;sdj&oacute;r</span><span class="pf-meta-info">'); print $husdjor_output; print t('</span></p>');endif;?>
            <?php if(isset($koliskap_output)) : print t('<p><span class="pf-meta-label"> K&oslash;lisk&aacute;p</span><span class="pf-meta-info">'); print $koliskap_output; print t('</span></p>');endif;?>
            <?php if(isset($ovnur_output)) : print t('<p><span class="pf-meta-label"> Ovnur</span><span class="pf-meta-info">'); print $ovnur_output; print t('</span></p>');endif;?>
            <?php if(isset($eymhetta_output)) : print t('<p><span class="pf-meta-label"> Eymhetta</span><span class="pf-meta-info">'); print $eymhetta_output; print t('</span></p>');endif;?>
            <?php if(isset($mikroovnur_output)) : print t('<p><span class="pf-meta-label"> Mikroovnur</span><span class="pf-meta-info">'); print $mikroovnur_output; print t('</span></p>');endif;?>
            <?php if(isset($uppvaskimaskina_output)) : print t('<p><span class="pf-meta-label"> Uppvaskimaskina</span><span class="pf-meta-info">'); print $uppvaskimaskina_output; print t('</span></p>');endif;?>
            <?php if(isset($vaskimaskina_output)) : print t('<p><span class="pf-meta-label"> Vaskimaskina</span><span class="pf-meta-info">'); print $vaskimaskina_output; print t('</span></p>');endif;?>
            <?php if(isset($turkitrumla_output)) : print t('<p><span class="pf-meta-label"> Turkitrumla</span><span class="pf-meta-info">'); print $turkitrumla_output; print t('</span></p>');endif;?>
            <?php if(isset($leiga_output)) : print t('<p><span class="pf-meta-label"> Leiga</span><span class="pf-meta-info">'); print $leiga_output; print t('</span></p>');endif;?>

            <?php /* <p><span class="pf-meta-label"><?php print t('Date');?></span><span class="pf-meta-info"><?php print date('d,M Y',$node->created);?></span></p>--> */ ?>
</td></tr></table>
        </div><!--end:pf-detail-meta-->
<?php if(strpos('ajax_node',current_path()) !== false):?>
    <?php if(isset($previous)):?>
        <a class="pf-detail-prev" href="#pf-detail-box" onclick="return boom_portfolio_detail_click(jQuery(this),<?php print $previous; ?>,false);"></a>
    <?php endif;?>
        <a class="pf-detail-hide"  onclick="return boom_portfolio_hide_click(jQuery(this));"></a>
    <?php if(isset($next)):?>
        <a class="pf-detail-next" href="#pf-detail-box" onclick="return boom_portfolio_detail_click(jQuery(this),<?php print $next; ?>,false);"></a>
    <?php endif;?>
<?php endif;?>
    </div> <!--end:pf-content-->
 <?php print render($content['field_pdf']);?>

</article><!--end:pf-detail-item-->
<script type="text/javascript">
    /***Portfolio Flex slider***/
    jQuery(document).ready(function($){
        jQuery('.flexslider').flexslider({
            animation: "slide",
            slideshow: true,
            start: function(slider){
                jQuery('body').removeClass('loading');
            }
        });
    });

</script>
