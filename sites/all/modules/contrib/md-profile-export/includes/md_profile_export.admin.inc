<?php
  /**
   * User: duynguyen
   * Date: 3/11/13
   * Time: 4:48 PM
   * To change this template use File | Settings | File Templates.
   */

  /**
   * _profile_export_form()
   */
  function md_profile_export_form($form, &$form_state)
  {
    module_load_include('inc', 'features', 'features.admin');
    features_include();
    $form = array();

    $form['profile_export'] = array(
      '#type' => 'fieldset',
      '#title' => t('Profile export info'),
      '#tree' => FALSE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['profile_export']['profile_name'] = array(
      '#type' => 'textfield',
      '#title' => t("Profile name"),
      '#description' => t("This text use to create name for profile"),
      '#default_value' => 'MD Boom',
      '#required' => TRUE,
    );

    $form['profile_export']['profile_desc'] = array(
      '#type' => 'textarea',
      '#title' => t("Description"),
      '#description' => t("Short description about profile will created."),
      '#default_value' => 'Create install profile for MD Boom Theme.',
      '#resizable' => FALSE,
      '#rows' => 2,
    );

    $default_modules = array();
    $enable_modules = module_list($refresh = FALSE, $bootstrap_refresh = TRUE);
    unset($enable_modules['md_profile_export']);
    foreach ($enable_modules as $module) {
      $default_modules[] = $module;
    }
    $form['profile_export']['profile_modules'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Modules enable'),
      '#options' => $enable_modules,
      '#default_value' => $default_modules,
    );

    $form['features_export'] = array(
      '#type' => 'fieldset',
      '#title' => t('Feature export'),
      '#tree' => FALSE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    # Generate form to create features export module
    $features_form = features_export_form($form, $form_state);
    $form['features_export']['info'] = $features_form['info'];
    $form['features_export']['info']['name']['#default_value'] = 'MD Boom Features Module';
    $form['features_export']['advanced'] = $features_form['advanced'];
    $form['features_export']['export'] = $features_form['export'];
    $form['#attributes'] = $features_form['#attributes'];
    $form['#attached'] = $features_form['#attached'];

    $form['features_export']['info']['module_name']['#element_validate'] = array("_profile_features_form_validate_field");
    $form['features_export']['info']['version']['#element_validate'] = array("_profile_features_form_validate_field");
    $form['features_export']['advanced']['project_status_url']['#element_validate'] = array("_profile_features_form_validate_field");
    $form['features_export']['advanced']['generate']['#submit'] = array("_profile_features_export_build_form_submit");
    $form['features_export']['advanced']['features_autodetect_wrapper']['features_refresh']['#submit'] = array("_profile_features_export_form_rebuild");
    $form['features_export']['advanced']['features_autodetect_wrapper']['features_refresh']['#ajax']['callback'] = "_profile_features_export_form_ajax";
    $form['features_export']['advanced']['features_allow_conflicts']['#ajax']['callback'] = "_profile_features_export_form_ajax";
    $form['features_export']['advanced']['info-preview']['#ajax']['callback'] = "_profile_features_info_file_preview";

    $form['download_profile'] = array(
      '#type' => 'submit',
      '#value' => t('Download profile'),
      '#submit' => array('md_profile_export_form_build_submit'),
    );

    return $form;
  }

  /**
   * Handle submit md_profile_export_form()
   */
  function md_profile_export_form_build_submit($form, &$form_state)
  {
    $configs = $form_state['values'];
    $profile_name = $configs['profile_name'];
    $features_module = $configs['module_name'];
    $enable_modules = $configs['profile_modules'];
    $folder_name = strtolower($profile_name);
    $folder_name = str_replace(' ', '_', $folder_name);

    # Create folder for profile
    $profile_dir_uri = "public://{$folder_name}";
    if (file_prepare_directory($profile_dir_uri, FILE_CREATE_DIRECTORY)) {
      file_unmanaged_delete_recursive($profile_dir_uri);
    }
    drupal_mkdir($profile_dir_uri);

    # Create modules folder
    $modules_dir = "{$profile_dir_uri}/modules";
    drupal_mkdir($modules_dir);

    # Create librares folder
    $libraries_path = DRUPAL_ROOT.'/sites/all/libraries';
    $libraries_dir = "{$profile_dir_uri}/libraries";
    drupal_mkdir($libraries_dir);
    _copy_directory($libraries_path, $libraries_dir);

    # Generate features module
    _generate_features_module($form, $form_state, $modules_dir);

    # Copy dependencies modules
    _copy_dependencies_modules($modules_dir, $form_state['values']['profile_modules']);
      # Generate install file
      _profile_generate_info_file($folder_name, $profile_name, $enable_modules, $features_module);

    # Copy theme directory
    $default_theme = variable_get('theme_default');
    drupal_mkdir("{$profile_dir_uri}/themes");
    _copy_directory(drupal_get_path('theme', $default_theme), "{$profile_dir_uri}/themes/{$default_theme}");

    # Generate sql file data
    drupal_mkdir("{$profile_dir_uri}/sql_dump");
    _profile_export_database("{$profile_dir_uri}/sql_dump", "database.data");


    _profile_generate_install_file($folder_name, $features_module);
    _profile_generate_profile_file($folder_name, $configs['profile_name']);

    # Copy all files needed
    _export_files($folder_name);

    # Create zip file
    //$zip = new ZipFolder(drupal_realpath("public://{$folder_name}.zip"), drupal_realpath($profile_dir_uri));
  }

  /**
   * Callback validate features export form field
   */
  function _profile_features_form_validate_field(&$element, &$form_state)
  {
    module_load_include('inc', 'features', 'features.admin');
    features_export_form_validate_field($element, $form_state);
  }

  /**
   * Form submission handler for features_export_form().
   */
  function _profile_features_export_build_form_submit($form, &$form_state)
  {
    module_load_include('inc', 'features', 'features.admin');
    features_export_build_form_submit($form['features_export'], $form_state);
  }

  /**
   * ajax form submission to rebuild form state for features_export_form
   */
  function _profile_features_export_form_rebuild($form, &$form_state)
  {
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Ajax handle for features_export_form
   */
  function _profile_features_export_form_ajax($form, &$form_state)
  {
    return $form['features_export']['export'];
  }

  /**
   * AJAX callback to get .info file preview for features module
   */
  function _profile_features_info_file_preview($form, &$form_state)
  {
    return $form['features_export']['export'];
  }

  /**
   * Generate features module for this profile
   */
  function _generate_features_module($form, $form_state, $directory = NULL)
  {
    # Prepare data for generate module
    $features_export = $form_state['values'];
    $features_export['op'] = $features_export['generate'];
    unset($features_export['profile_name']);
    unset($features_export['profile_desc']);
    unset($features_export['profile_modules']);
    unset($features_export['download_profile']);

    if ($features_export['generate_path'] == '' && $directory == NULL) {
      $features_export['generate_path'] = 'sites/default/files';
    } else {
      $features_export['generate_path'] = ($features_export['generate_path'] != '') ? $features_export['generate_path'] : $directory;
    }

    $feature_form_state = $form_state;
    $feature_form_state['values'] = $features_export;
    $form['features_export']['#feature'] = NULL;

    # Generate module
    module_load_include('inc', 'features', 'features.admin');
    features_export_build_form_submit($form['features_export'], $feature_form_state);
  }

  /**
   * Copy modules dependencies in profile
   */
  function _copy_dependencies_modules($profile_modules_dir, $enable_modules = array())
  {
    #
    $modules_object = system_rebuild_module_data();
    $package_modules = array();

    # Organize module by package
    foreach ($modules_object as $module) {
      if (in_array($module->name, $enable_modules)) {
        $package_modules[$module->info['package']][] = $module;
      }
    }

    # Get path of all modules need copy
    $modules_path = array();
    foreach ($package_modules as$package_name => $modules) {
      if (strtolower($package_name) == 'core')
        continue;

      foreach ($modules as $module) {
        $module_path_elements = explode('/', $module->uri);
        $end_offset = array_search('modules', $module_path_elements);
        if ($end_offset !== FALSE && $end_offset >= 2) {
          if (in_array('contrib', $module_path_elements) || in_array('custom', $module_path_elements)) {
            $end_offset += 3;
            $path = implode('/', array_slice($module_path_elements, 0, $end_offset));
          } else {
            $end_offset += 2;
            $path = implode('/', array_slice($module_path_elements, 0, $end_offset));
          }

          if (!in_array($path, $modules_path)) {
            $modules_path[$module_path_elements[$end_offset - 1]] = $path;
          }
        }
      }
    }

    # Copy modules
    foreach ($modules_path as $name => $path) {
      _copy_directory($path, "{$profile_modules_dir}/{$name}");
    }
  }