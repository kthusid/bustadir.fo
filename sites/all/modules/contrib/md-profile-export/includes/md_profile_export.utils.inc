<?php
/**
 * Created by JetBrains PhpStorm.
 * User: duy
 * Date: 3/16/13
 * Time: 10:23 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * convert array block to string code define array
 */
function _convert_array_to_code_define($array, &$level) {
  $level++;
  $tabs = "";
  $tabs_close = "";
  for ($i = 0; $i < $level; $i++) {
    $tabs .= "  ";
    if ($i < $level-1)
      $tabs_close .= "  ";
  }
  $output = "array(\n";

  foreach ($array as $key => $val) {
    if (is_string($key))
      $key = "'{$key}'";

    if (is_array($val)) {
      $output .= $tabs . "{$key} => " . _convert_array_to_code_define($val, $level) . ",\n";
      $level--;
      continue;
    }

    # Generate for normal data type
    if (is_string($val))
      $val = '"' . $val .'"';
    $output .= $tabs . "{$key} => {$val},\n";
  }

  $output .= $tabs_close . ")";
  return $output;
}

  /**
   * Copy directory function
   */
  function _copy_directory($source, $destination, $exclude = array('.svn', 'CVS', '.git')) {
    # Remove all subversion files
    $exclude[] = '.svn';
    $exclude[] = '.git';
    $exclude[] = 'CVS';
    $exclude = array_unique($exclude);

    # Remove slash sign end of source path
    $source = rtrim($source, '/');

    # Create destination directory
    if (file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      file_unmanaged_delete_recursive($destination);
    }
    drupal_mkdir($destination);

    # List all files and directory in source
    $directory = dir($source);
    while (FALSE !== ($file = $directory->read())) {
      # Check file is current directory?
      if ($file == '.' || $file == '..' || in_array($file, $exclude))
        continue;

      # Create file path
      $file_path = "{$source}/{$file}";

      # Check file is subdirectory
      if (is_dir($file_path)) {
        # Copy subdirectory
        _copy_directory($file_path, "{$destination}/{$file}");
        continue;
      }

      # Copy file
      file_unmanaged_copy($file_path, "{$destination}/{$file}");
    }

    $directory->close();
  }


  /**
   * Generate sql dump file content
   */
  function _generate_sql_dump($tables = array()) {
    global $database;
    $table_prefix = $database['default']['default']['prefix'];
    $file_content = "";
    foreach ($tables as $table_name) {
      $records = db_select($table_name, 'all_fields')
        ->fields('all_fields')
        ->execute()
        ->fetchAll();

      if (empty($records))
        continue;

      $file_content .= "/*Table structure for table `{$table_name}` */\n\n";
      $file_content .= "DROP TABLE IF EXISTS {$table_name};\n\n";

      $create_query = mysql_fetch_row(mysql_query("SHOW CREATE TABLE {$table_prefix}{$table_name}"));
      $create_query = $create_query[1];

      if ($table_prefix != '') {
        str_replace($table_prefix, '', $create_query);
      }

      # Create string fields values in insert command
      $record = (array)$records[0];
      $fields = array();
      foreach (array_keys($record) as $field) {
        $fields[] = "`{$field}`";
      }
      $fields = implode(',' , $fields);

      $file_content .= "{$create_query}\n\n";
      $file_content .= "LOCK TABLES `{$table_name}` WRITE;";
      $file_content .= "INSERT INTO `{$table_name}`({$fields}) VALUES ";

      $records_values = array();
      foreach ($records as $record) {
        foreach ($record as &$val) {
          $val = "'{$val}'";
        }
        $value_str = implode(',', (array)$record);
        $records_values[] = "({$value_str})";
      }

      $file_content .= implode(',', $records_values) . ";\n";
      $file_content .= "UNLOCK TABLES;\n\n";
    }

    return $file_content;
  }

/**
 * Copy files and image to profile export
 */
function _export_files($profile_name) {
  $path = drupal_realpath('public://');
  $save_dir = "public://{$profile_name}/files";
  _copy_directory($path, $save_dir, array('.htaccess', 'styles', 'ctools', $profile_name));
  $files = _profile_ja_files($save_dir,  "\.(jpg|png|jpeg|gif)$", true, true);

  if (count($files)) {
    foreach ($files as $file) {
      list($width, $height, $type, $attr) = getimagesize($file);
      $ext = _get_file_extension($file);

      switch ($ext) {
        case 'gif':
          imagegif(imagecreatefromstring(file_get_contents('http://placehold.it/'.$width.'x'.$height)), drupal_realpath($file));
          break;
        case 'png':
          imagepng(imagecreatefromstring(file_get_contents('http://placehold.it/'.$width.'x'.$height)), drupal_realpath($file));
          break;
        case 'jpg':
        case 'jpeg':
          imagejpeg(imagecreatefromstring(file_get_contents('http://placehold.it/'.$width.'x'.$height)), drupal_realpath($file));
          break;
      }
    }
  }
}

/**
 * Get file extension
 */
function _get_file_extension($file) {
  $ext = strtolower(substr($file, strrpos($file, '.')+1));
  return $ext;
}

  function _profile_ja_files($path, $filter = '.', $recurse = false, $fullpath = false, $exclude = array('.svn', 'CVS')) {
    $exclude[] = '.svn';
    $exclude[] = '.git';
    $exclude[] = 'CVS';

    $exclude = array_unique($exclude);

    $arr = _ja_files($path, $path, $filter, $recurse, $fullpath, $exclude);
    if(!$arr) {
      $arr = array();
    }

    return $arr;
  }

  function _ja_files($path, $basePath, $filter = '.', $recurse = false, $fullpath = false, $exclude = array('.svn', 'CVS')) {
    # Initialize variables
    $arr = array();

    # Is the path a folder?
    if (!is_dir($path)) {
      watchdog('profile_export', '@Path is not a folder.', array('@Path' => $path));
      return false;
    }

    # read the source directory
    $handle = opendir($path);
    while (($file = readdir($handle)) !== false) {
      if (($file != '.') && ($file != '..') && (!in_array($file, $exclude))) {
        $dir = "{$path}/{$file}";

        if (is_dir($dir)) {
          if ($recurse) {
            if (is_integer($recurse)) {
              $arr2 = _ja_files($dir, $basePath, $filter, $recurse - 1, $fullpath, $exclude);
            } else {
              $arr2 = _ja_files($dir, $basePath, $filter, $recurse, $fullpath, $exclude);
            }

            $arr = array_merge($arr, $arr2);
          }
        } else {
          if (preg_match("/$filter/i", $file)) {
            if ($fullpath) {
              $arr[] = $dir;
            } else {
              $arr[] = $file;
            }
          }
        }
      }
    }
    closedir($handle);

    asort($arr);
    return $arr;
  }